import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PadreComponent } from '../padre/padre.component';

@Component({
  selector: 'app-figlio',
  templateUrl: './figlio.component.html',
  styleUrls: ['./figlio.component.css'],
})
export class FiglioComponent {
  @Input() romanarray!: string[];

  dispari!: string[];

  onEvent(event: any) {
    this.romanarray = event;
  }
}
