import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css'],
})
export class PadreComponent {
  @Output() sendArray = new EventEmitter<number[]>();
  // dispari: string[] = ['I', 'III', 'V', 'VII', 'IX'];
  dispari: number[] = [1, 3];

  //  sendEvent() {
  //     for (let stringa of this.dispari) {
  //       this.sendArray.emit(stringa); /// per riuscire a far passare tutto l array di stringhe che senno' dava errore
  //     }
  //   }

  sendEvent() {
    this.sendArray.emit(this.dispari);
  }
}
